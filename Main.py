# -*- coding: utf-8 -*-

from Topic import Topic
import re

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO


# def read_data():
#    file = open("demo.txt", "r")
#    return file.read().decode('utf-8').lower().encode('utf-8')


def content_ok(text, tpObj1, tpObj2):
    for tpObj in topicObjs:
        if tpObj.get_topic() != tpObj1.get_topic() and tpObj.get_topic() != tpObj2.get_topic():
            keyList = tpObj.get_key()
            for key in keyList:
                regex = r"" + key + "\s*:?\n+"
                pattern = re.compile(regex, re.IGNORECASE | re.UNICODE)
                if pattern.search(text):
                    return False
    return True


def find_content(text, tpObj1, tpObj2):
    keyList1 = tpObj1.get_key()
    keyList2 = tpObj2.get_key()
    for key1 in keyList1:
        for key2 in keyList2:
            regex = r"" + key1 + "\s*:?\n+([.\s\S]*)\n+" + key2
            # print(regex)
            pattern = re.compile(regex, re.IGNORECASE | re.UNICODE)
            if pattern.search(text):
                # print("tim thay " + key1 + " " + key2)
                match = pattern.search(text)
                content = match.group(1)
                if content_ok(content, tpObj1, tpObj2):
                    # print("tim thay " + key1 + " " + key2)
                    return True, content
    return False, ""


def extract_topic(text):
    for tpObj1 in topicObjs:
        for tpObj2 in topicObjs:
            if tpObj1.get_topic() != tpObj2.get_topic():
                check, content = find_content(text, tpObj1, tpObj2)
                if check:
                    tpObj1.set_content(content)


def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = file(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                  check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text


topics = [
    "thông tin cá nhân",
    "mục tiêu nghề nghiệp",
    "học vấn",
    "kinh nghiệm làm việc",
    "kỹ năng",
    "chứng chỉ",
    "giải thưởng",
    "hoạt động",
    "sở thích",
    "thông tin khác",
    "unknown"
]

keys = {
    "thông tin cá nhân": ["thông tin cá nhân", "thông tin liên hệ"],
    "mục tiêu nghề nghiệp": ["mục tiêu nghề nghiệp"],
    "học vấn": ["học vấn", "trình độ", "quá trình học tập"],
    "kinh nghiệm làm việc": ["Dự án - kinh nghiệm làm việc", "kinh nghiệm làm việc", "kinh nghiệm", "dự án"],
    "kỹ năng": ["các kỹ năng", "kỹ năng", "kỹ năng công việc"],
    "chứng chỉ": ["chứng chỉ"],
    "giải thưởng": ["giải thưởng"],
    "hoạt động": ["hoạt động", "hoạt động ngoại khóa"],
    "sở thích": ["sở thích"],
    "thông tin khác": ["thông tin khác"],
    "unknown": ["unknown"]
}

# Create topic objects
topicObjs = []
for topic in topics:
    topicObj = Topic(topic, keys[topic])
    topicObjs.append(topicObj)

# Read data
text = convert_pdf_to_txt("data/PhamVanLong.pdf")
text = text.decode('utf-8').lower().encode('utf-8') + "\nunknown"
print(text)
extract_topic(text)

for topicObj in topicObjs:
    print("{\n" + " topic: " + topicObj.get_topic() + "\n" + " text: " + topicObj.get_content() + "\n" + "}\n")
