class Topic:
    def __init__(self, topic, key):
        self.topic = topic
        self.key = key
        self.content = ""

    def get_topic(self):
        return self.topic

    def get_key(self):
        return self.key

    def get_content(self):
        return self.content

    def set_topic(self, topic):
        self.topic = topic

    def set_key(self, key):
        self.key = key

    def set_content(self, content):
        self.content = content
